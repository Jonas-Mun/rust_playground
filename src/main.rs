use std::env;
use std::fs;
use std::process;
use std::error::Error;
use std::io::prelude::*;

struct Config {
    query: String,
    filename: String,
}

impl Config {
    fn new(args: Vec<String>) -> Result<Config, &'static str> {
        if args.len() < 2 {
            return Err("Not enough arguments");
        }

        let query = args[1].clone();
        let filename = args[2].clone();

        Ok(Config { query, filename, })
    }

}

fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let txt_string = fs::read_to_string(config.filename)?;

    println!("{}", txt_string);

    let mut fw = fs::File::create("to_write.txt")?;
    fw.write_all(b"We have gained knowledge")?;

    Ok(())

}

fn main() {
    let arg: Vec<String> = env::args().collect();

    let config = Config::new(arg).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = run(config) {
        println!("Application error: {}", e);

        process::exit(1);
    }

}

